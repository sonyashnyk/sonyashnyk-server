class CreateAdvts < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.integer :role, default: 0
      t.string :name
      t.string :email
      t.string :phone

      t.timestamps null: false
    end

    create_table :advts do |t|
      t.references :author, foreign_key: { to_table: :users }, null: false
      t.integer :category, default: 0
      t.integer :beneficiary, default: 0
      t.text :description
      t.text :proofs
      t.boolean :verified, default: false
      t.string :city, index: true

      t.timestamps null: false
    end
  end
end
