class User < ApplicationRecord
  has_many :advts

  enum role: [:user, :admin]
end