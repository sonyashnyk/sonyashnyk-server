class Advt < ApplicationRecord
  belongs_to :user

  enum category: [ :finance, :volunteering, :search, :aid ]
  enum beneficiary: [ :army, :defense, :civilians, :kids, :nature ]
end
