FactoryBot.define do
  factory :advts do
    user
    description FFaker::Lorem.paragraph
    proofs FFaker::Internet.http_url
    city "Kharkiv"
  end
end