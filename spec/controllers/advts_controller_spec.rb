require 'rails_helper'

RSpec.describe AdvtsController do
  describe "GET #index" do
    it "returns all advts" do
      get :index, params: {}, format: :json

      expect(response).to have_http_status(200)
    end
  end
end
