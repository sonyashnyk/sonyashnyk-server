FactoryBot.define do
  factory :users do
    name FFaker::Name.unique.name
    email FFaker::Internet.email
    role 1
  end
end